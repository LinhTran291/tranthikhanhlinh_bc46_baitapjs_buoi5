// Them cac phan tu vao mang
var arrayNumber = [];

function themso() {
  var number = document.querySelector("#number").value * 1;
  document.querySelector("#number").value = "";
  arrayNumber.push(number);
  console.log(arrayNumber);
  var contentHtml = `${arrayNumber}`;
  document.querySelector("#resultbai1").innerHTML = contentHtml;
}

// tinh tong cac phan tu trong mang
function tinhtongsoduong() {
  var tongMang = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] > 0) {
      tongMang += arrayNumber[i];
    }
  }

  //   console.log(tongMang);
  document.querySelector("#resultbai2").innerHTML = `${tongMang}`;
}

// Dem co bao nhieu so duong trong mang
function demsoduong() {
  var count = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] > 0) {
      count += 1;
    }
  }
  console.log(count);
  document.querySelector("#resultbai3").innerHTML = `${count}`;
}

//Tim so nho nhat trong mang
function timsonhonhat() {
  var soMin = arrayNumber[0];
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] < soMin) {
      soMin = arrayNumber[i];
    }
  }
  console.log(soMin);
  document.querySelector("#resultbai4").innerHTML = `${soMin}`;
}

// tim so duong nho nhat trong mang
function soduongnhonhat() {
  var arraySoDuong = [];
  var count = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] > 0) {
      count++;
      arraySoDuong.push(arrayNumber[i]);
    }
  }
  if (count > 0) {
    var min = arraySoDuong[0];
    for (var i = 0; i < arraySoDuong.length; i++) {
      if (arraySoDuong[i] < min) {
        min = arraySoDuong[i];
      }
    }
    document.querySelector("#resultbai5").innerHTML = `${min}`;
  } else {
    document.querySelector(
      "#resultbai5"
    ).innerHTML = `Mảng vừa nhập không có số dương`;
  }
}

//Tim so chan cuoi cung trong mang neu mang khong co so chan thi tra ve gia tri -1
function timsochan(arrayNumber) {
  var countChan = 0;
  for (var i = 0; i < arrayNumber.length; i++) {
    if (arrayNumber[i] % 2 == 0) {
      countChan += 1;
      return countChan;
    }
  }
  return 0;
}
function sochancuoicung() {
  var countChan = timsochan(arrayNumber);
  if (countChan == 0) {
    return -1;
  } else {
    for (var i = arrayNumber.length - 1; i >= 0; i--) {
      if (arrayNumber[i] % 2 == 0) {
        var soChan = arrayNumber[i];
        console.log(soChan);
        document.querySelector("#resultbai6").innerHTML = `${soChan}`;
        return soChan;
      }
    }
  }
}

// Doi cho 2 gia tri trong mang theo vi tri (Cho nhap vao 2 vi tri muon doi cho gia tri)
function hoandoivitri(vitri1, vitri2) {
  var vitri1 = document.getElementById("vitri1").value * 1;
  var vitri2 = document.getElementById("vitri2").value * 1;
  let temp;
  temp = arrayNumber[vitri1];
  arrayNumber[vitri1] = arrayNumber[vitri2];
  arrayNumber[vitri2] = temp;
  document.querySelector("#resultbai7").innerHTML = `${arrayNumber}`;
  console.log(arrayNumber);
}

//Sap xep mang tang dan
function sapxeptangdan() {
  arrayNumber.sort(function (a, b) {
    return a - b;
  });
  document.querySelector("#resultbai8").innerHTML = `${arrayNumber}`;
  console.log(arrayNumber);
}

// Tim so nguyen to dau tien trong mang, neu khong co in ra so -1
function songuyento(a) {
  var count = 0;
  for (var i = 1; i <= a; i++) {
    if (a % i == 0) {
      count += 1;
    }
  }
  if (count == 2) {
    return 1;
  } else {
    return -1;
  }
}
function songuyentodautien() {
  let flag, soNguyenToDauTien;
  for (var i = 0; i < arrayNumber.length; i++) {
    flag = songuyento(arrayNumber[i]);
    if (flag == 1) {
      soNguyenToDauTien = arrayNumber[i];
      document.querySelector("#resultbai9").innerHTML = `${soNguyenToDauTien}`;
      console.log(soNguyenToDauTien);
      return soNguyenToDauTien;
    }
  }
  if (flag != 1) {
    document.querySelector("#resultbai9").innerHTML = `-1`;
    console.log("-1");
  }
}

//
var arrayFloatNumber = [];
function themsofloat() {
  var number = document.getElementById("floatnumber").value * 1;
  document.querySelector("#floatnumber").value = "";
  arrayFloatNumber.push(number);
  document.querySelector("#resultadd").innerHTML = `${arrayFloatNumber}`;

  console.log(arrayFloatNumber);
}
function sothuc() {
  var count = 0;
  for (var i = 0; i < arrayFloatNumber.length; i++) {
    if (Number.isInteger(arrayFloatNumber[i]) == true) {
      count += 1;
    }
  }
  document.querySelector(
    "#resultbai10"
  ).innerHTML = `Số lượng số nguyên trong mảng là: ${count}`;

  console.log(count);
}

function sosanh() {
  var soDuong = 0;
  var soAm = 0;
  for (var i = 0; i < arrayFloatNumber.length; i++) {
    if (arrayFloatNumber[i] > 0) {
      soDuong += 1;
    } else if (arrayFloatNumber[i] < 0) {
      soAm += 1;
    }
  }
  if (soDuong == soAm) {
    document.querySelector("#resultbai11").innerHTML = `Số dương = Số âm`;
  } else if (soDuong > soAm) {
    document.querySelector("#resultbai11").innerHTML = `Số dương > Số âm`;
  } else {
    document.querySelector("#resultbai11").innerHTML = `Số dương < Số âm`;
  }
}
